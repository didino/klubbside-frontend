import {Component} from 'angular2/core';
import {ROUTER_DIRECTIVES, RouterOutlet} from "angular2/router";
import {RouteConfig} from "angular2/router";
import {PostComponent} from "./post/post.component";
import {TeamMemberComponent} from "./teammember/teammember.component";
import {TeamComponent} from "./team/team.component";
import {ClubComponent} from "./club/club.component";
import {StoreComponent} from "./store/store.component";
import {FixtureComponent} from "./fixture/fixture.component";
import {ContactComponent} from "./contact/contact.component";
import {SponsorComponent} from "./sponsor/sponsor.component";

@Component({
    selector: 'my-app',
    templateUrl: 'app/master.html',
    styleUrls: ['app/sidebar.css'],
    directives: [ROUTER_DIRECTIVES, RouterOutlet]
})
@RouteConfig([
    {path: '/nyheter', name: 'Post', component: PostComponent, useAsDefault: true},
    {path: '/klubben', name: 'Club', component: ClubComponent},
    {path: '/lagene/', name: 'Team', component: TeamComponent },
    {path: '/spillere', name: 'TeamMember', component: TeamMemberComponent},
    {path: '/kamper', name: 'Fixture', component: FixtureComponent},
    {path: '/butikk', name: 'Store', component: StoreComponent},
    {path: '/kontaktinfo', name: 'Contact', component: ContactComponent},
    {path: '/sponsorer', name: 'Sponsor', component: SponsorComponent}
])
export class AppComponent {
    public isToggled = false;
}